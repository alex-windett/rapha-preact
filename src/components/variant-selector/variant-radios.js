import { h, Component } from 'preact';
import styled from 'styled-components'

import RadioButton from '@ui/radio-button';
import H4 from '@ui/h4';
import { styles } from '@constants';

const StyleColorSpan = styled.span`
	color: ${styles.colors.grey}
`

const VariantRadios = ({
	colors = {},
	onChange,
	name = '',
	selectedValue = ''
}) =>  {

	const variants = Object.keys(colors).map( c => {

		const color = colors[c];

		return (
			<RadioButton
				name={name}
				value={color.sku}
				swatchColor={color.inlineStyleSwatchColor}
				checked={selectedValue === color.sku}
				onChange={onChange}
			/>
		)
	});

	const selectedColorLabel = selectedValue ? colors[selectedValue].color : '';

	return (
		<div className={"swatches"}>
			<H4>Colors <StyleColorSpan>{selectedColorLabel}</StyleColorSpan></H4>
			{variants}
		</div>
	)
};

export default VariantRadios;
