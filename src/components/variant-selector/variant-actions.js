import { h, Component } from 'preact';
import styled  from 'styled-components';
import H4 from '@ui/h4';

import Button from '@ui/button';

const WasPrice = styled.h4`
	text-decoration: line-through;
	color: grey;
`;

const variantActions = ({
	selectedVariant = {
		wasPrice: {},
		price: {}
	},
	selectedSize,
	onSaveForLaterSubmit,
	onSizeClick,
	onProductSubmit,
	onNotifySubmit
}) =>  {

	const { sizes } = selectedVariant;

	const notifyMe = !selectedSize ? Object.values(sizes).reduce((acc, curr) => acc ? !acc.notifyMe : curr.notifyMe, false) : selectedSize.notifyMe;
	const outOfStock = !selectedSize ? Object.values(sizes).reduce((acc, curr) => acc ? acc.stock : curr.stock < 1, false) : selectedSize.stock < 1;

	const submitText = notifyMe && outOfStock
		? 'Notify Me When Available'
		: outOfStock
			? 'Out Of Stock'
			: 'Add To Basket';

	const isDisabled = notifyMe && outOfStock
		? false
		: outOfStock;

	return (
		<div>
			<Button link onClick={onSaveForLaterSubmit}>Save For Later</Button>

			<Button link onClick={onSizeClick}>Size Guide</Button>

			<H4>{selectedVariant.price ? selectedVariant.price.formattedValue : null}</H4>
			<WasPrice>was price{selectedVariant.wasPrice ? selectedVariant.wasPrice.formattedValue : null}</WasPrice>

			<Button disabled={isDisabled} onClick={notifyMe ? onNotifySubmit : onProductSubmit}>{submitText}</Button>
		</div>
	)
}

export default variantActions
