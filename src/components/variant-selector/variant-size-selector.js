import { h, Component } from 'preact';
import styled from 'styled-components'

import Dropdown from '@ui/Dropdown';

const VariantSizeSelector = ({
   onChange,
   selectedValue = '',
   checkedValue = '',
   variant = {}
}) =>  {

	return (
		<div>
			<h3>Size</h3>
			<Dropdown
				onChange={onChange}
				options={variant.sizes}
				defaultValue={"Please select a size"}
				checkedValue={checkedValue}
				defaultSelected={selectedValue}
			/>
		</div>
	);
};

export default VariantSizeSelector;
