import { h, Component } from 'preact';
import styled from 'styled-components';

const StyledWrapper = styled.section`
	max-width: 90rem;
	margin: 0 auto;
	display: flex;
	flex-direction: row;
    flex-wrap: wrap;
	align-items: center;
	
    > div {
	   flex: 1 0 50%;
	}
	
	> div:last-child {
		flex: 0 1 100%;
		display: flex;
		justify-content: space-around;
		align-items: center;
	}
`;

const variantWrapper = ({
	children
}) =>  {

	return (
		<StyledWrapper>
			{children}
		</StyledWrapper>
	)
}

export default variantWrapper;
