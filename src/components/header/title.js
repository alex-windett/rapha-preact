import styled from 'styled-components'
import { h, Component } from 'preact';

import H3 from '@ui/h3';

export default ({children }) => <H3>{children}</H3>
