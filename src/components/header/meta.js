import styled from 'styled-components'
import { h, Component } from 'preact';
import Button from '@ui/button';

const StyledMeta = styled.div`
	display: flex;
    justify-content: space-around;
	align-items: center;
`;

const WasPrice = styled.span`
	color: grey;
	text-decoration: line-through;
`;

const HeaderMeta = ({
	buttonDisabled = false,
	buttonOnClick,
	buttonText = '',
	price = '',
	wasPrice = '',
	selectedSize = ''
}) => {

	const scrollDownPage = () => console.log('scroll down the page');

	const _buttonOnClick = () => selectedSize.sku ? buttonOnClick(selectedSize.sku) : scrollDownPage();

	return (
		<StyledMeta>
			<div>
				<span>{price}</span>
				<WasPrice>{wasPrice ? <span>{wasPrice}</span> : null}</WasPrice>
			</div>
			<Button type="button" disabled={buttonDisabled} onClick={_buttonOnClick} tertiary>
				{buttonText}
			</Button>
		</StyledMeta>
	)
}

export default HeaderMeta
