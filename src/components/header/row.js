import styled from 'styled-components'
import { h, Component } from 'preact';

const StyledHeader = styled.header`
    width: 100%;
    background-color: #000;
    color: #fff;
    padding: 1.5rem 0;
	display: flex;
    justify-content: space-between;
    align-items: center;
`;

const Header = ({children, classNames = ''}) => {

	return (
		<StyledHeader classNames={classNames}>
			{children}
		</StyledHeader>
	)
}

export default Header
