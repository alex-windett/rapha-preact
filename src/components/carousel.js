import styled from 'styled-components'
import { h, Component } from 'preact';
import SliderCarousel from '@ui/carousel';

const Carousel = ({ selectedSlide, images }) =>  {

	const slides = images.map( (image, i ) => ({
		"sku": image.variant,
		"title": image.title,
		"url": image.url
	}));

	return (
		<SliderCarousel slides={slides} selectedSlide={selectedSlide}/>
	);
}

export default Carousel;
