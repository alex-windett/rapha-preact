import { h, Component } from 'preact';
import styled from 'styled-components'
var Carousel = require('react-responsive-carousel').Carousel;
import 'react-responsive-carousel/lib/styles/carousel.css';

import Slide from './slide';

const _Carousel = ({slides, selectedSlide}) => {

	return (
		<Carousel
			showArrows={true}
			showStatus={false}
			showThumbs={false}
			infiniteLoop={true}
			selectedItem={selectedSlide}
		>
			{slides.map( slide => <Slide {...slide} />)}
		</Carousel>
	)
};


export default _Carousel;
