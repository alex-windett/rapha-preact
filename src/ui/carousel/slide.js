import { h, Component } from 'preact';

const Slide = ({id, title, url, current}) => (
	<div key={id}>
		<img src={url} />
	</div>
)

export default Slide
