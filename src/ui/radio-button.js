import styled from 'styled-components'
import { h, Component } from 'preact';

const StyledLabel = styled.label`
	& + & {
    	margin-left: 1rem;
    }
`;

const StyledInput = styled.input`
	background-color: ${props => props.swatchColor};
	width: 30px;
    height: 30px;
    border-radius: 100%;
    display: inline-block;
    border: 1px solid rgba(0, 0, 0, 0.15);
    margin: 0;
    appearance: none;
    outline: none;
    
    &:checked {
    	border: 1px solid black; 
    }
`;

const StyledSpan = styled.span``;

const RadioButton = ({
	name = '',
	value = '',
	dataId = '',
	dataIndex = 0,
	checked = false,
 	swatchColor = '',
	onChange,
	label = ''
}) => {

	return (
		<StyledLabel>
			<StyledInput
				onChange={onChange}
				swatchColor={swatchColor}
				type="radio"
				name={name}
				value={value}
				data-id={dataId}
				data-index={dataIndex}
				checked={checked} />
			<StyledSpan>{label}</StyledSpan>
		</StyledLabel>
	)
}

export default RadioButton;
