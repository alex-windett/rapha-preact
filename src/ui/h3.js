import styled from 'styled-components'
import { h, Component } from 'preact';

const StyledHeader = styled.h3`
	font-size: 2.4rem;
    line-height: 2.4rem;
`

export default ({children}) => (
	<StyledHeader>
		{children}
	</StyledHeader>
)
