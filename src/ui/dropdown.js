import styled from 'styled-components'
import { h, Component } from 'preact';

import { styles } from '@constants';

const StyleSelect = styled.select`
	border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 0;
    width: 100%;
    font-size: 1.6rem;
    font-family: "Adobe Caslon", serif;
    padding: 0.75rem 0.75rem calc(0.5rem - 1.5px);
    margin-top: ${styles.dim4};
    background-repeat: no-repeat;
    background-position: calc(100% - 1rem) center;
    background-size: 10px auto;
    background-color: none;
    padding-right: ${styles.dim2};
    appearance: none;
`;

const Dropdown = ({
	name = '',
	defaultValue = '',
	checkedValue = '',
	children,
	options = '',
	onChange
 }) => {

	const _options = Object.keys(options).map( option => {
		const _option = options[option];

		return (
			<option selected={_option.sku === checkedValue} value={_option.sku}>{_option.size}</option>
		)
	})

	return (
		<StyleSelect name={name} onChange={onChange}>
			<option value="" selected={!checkedValue} disabled>
				{defaultValue}
			</option>

			{_options}
		</StyleSelect>
	)
}

export default Dropdown;
