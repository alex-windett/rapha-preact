import styled from 'styled-components'
import { h, Component } from 'preact';

const StyledHeader = styled.h4`
	font-size: 1.4rem;
    line-height: 1.4rem;
`

export default ({children}) => (
	<StyledHeader>
		{children}
	</StyledHeader>
)
