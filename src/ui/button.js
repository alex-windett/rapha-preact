import styled from 'styled-components'
import { h, Component } from 'preact';

import { styles } from '@constants'

const {
	colors,
	dims
} = styles;

const StyledButton = styled.button`
	box-sizing: border-box;
    border: 1px solid ${props => props.secondary ? 'black' : props.link ? 'transparent' : 'white' };
    border-radius: 0;
    cursor: pointer;
    font-size: ${styles.dim1};
    line-height: 1;
    text-transform: uppercase;
    padding: calc(1.25rem - 1px) 2rem;
    min-width: ${styles.dim4};
    text-align: center;
    transition: background-color 0.2s ease-in-out, border 0.2s ease-in-out, color 0.2s ease-in-out;
    color: ${props => props.secondary ? 'black' : props.link ? 'black' : 'white' };
    background-color: ${props => props.secondary ? 'transparent' : props.tertiary ? 'transparent' : props.link ? 'transparent' : 'black' };
    ${props => props.link ? 'text-decoration: underline;' : ''}
    
    &:disabled {
    	opacity: 0.3;
    }
    
    &:hover:not(&:disabled) {
		border: 1px solid ${props => props.secondary ? colors.pink : props.link ? 'transparent' : props.tertiary ? colors.pink : 'transparent' };
		color: ${props => props.secondary ? colors.pink : props.link ? 'transparent' : props.tertiary ? colors.pink : colors.white };
		background-color: ${props => props.secondary ? 'transparent' : props.tertiary ? 'transparent' : props.link ? 'transparent' : colors.pink };
	}
`;

const Button = ({
	onClick,
	href,
	target = '',
	children,
	secondary = false,
	tertiary = false,
	link = false,
	disabled = false
}) =>  (
	<StyledButton link={link} secondary={secondary} tertiary={tertiary} href={href} onClick={onClick} target={target} disabled={disabled}>
		{children}
	</StyledButton>
);

export default Button;
