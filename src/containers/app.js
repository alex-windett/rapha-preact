// theirs
import { h, Component } from 'preact';
import { connect } from 'react-redux';
import styled  from 'styled-components';

import { setCurrentVariant, findSizeFromSKU, setProductOnLoad } from '@actions';
import VariantSelector from '@containers/variant-selector';

const StyledAppWrapper = styled.div`
	font-family: 'Oswald', sans-serif;
	font-size: 62.5%;
	
	* {
		font-family: 'Oswald', sans-serif;
	}
`;


class Widget extends Component {
	state = {};
	static defaultProps = {};

	componentDidMount() {
		this.props.setProductOnLoad(this.props.product)
	}

	render(props) {

		const {
			product
		} = props;

		return (
			<StyledAppWrapper>
				<VariantSelector product={product} />
			</StyledAppWrapper>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	setProductOnLoad: product => dispatch(setProductOnLoad(product))
})

const mapStateToProps = state => ({
	currentVariant: setCurrentVariant(state)
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Widget);
