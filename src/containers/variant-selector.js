import { h, Component } from 'preact';
import { connect } from 'react-redux';

import { VariantRadios, VariantSizeSelector, VariantActions, VariantWrapper } from "@components/variant-selector";
import { handleSizeChange, handleVariantChange, setCurrentVariant, setProductOnLoad, setSelectedSlide } from '@actions';
import EcommerceBar from "./ecommerce-bar";
import VariantsCarousel from "@components/carousel";

class VarinatSelector extends Component {

	constructor(props) {
		super(props)
	}

	componentWillMount() {
		const {
			product,
			setCurrentVariant,
			setProductOnLoad,
			setSelectedSlide
		} = this.props;

		const {
			galleryImages,
			selectedProductCode,
			colors
		} = product;

		const defaultVariant = colors[Object.keys(colors)[0]];

		let currentVariant = Object.values(colors).filter( color => selectedProductCode.includes(color.sku));
				currentVariant = currentVariant[0] || defaultVariant;

		const selectedSlide = galleryImages.findIndex( img => selectedProductCode.includes(img.variant))

		setProductOnLoad(product);
		setCurrentVariant(currentVariant);
		setSelectedSlide(selectedSlide);
	}

	onVariantChange(e) {

		const {
			product,
			selectedSize,
			setSelectedSlide,
			handleVariantChange
		} = this.props;

		const slide = product.galleryImages.findIndex( img => img.variant === e.target.value)

		setSelectedSlide(slide);
		handleVariantChange(e.target.value, selectedSize.sanitizedSize);
	}

	onSizeChange(e) {
		this.props.handleSizeChange(e.target.value);
	}

	onProductSubmit(sku) {
		console.log('Ajax request to submit here with product', sku)
	}

	onSaveForLaterSubmit(sku) {
		console.log('Ajax request to save for later here with product', sku)
	}

	onNotifySubmit(sku) {
		console.log('Ajax request to notify ne here with product', sku)
	}

	render({
		product,
		selectedVariant,
		selectedSize,
		selectedSlideIndex
	}) {

		const {
			title,
			colors,
			galleryImages,
			selectedProductCode
		} = product;

		const checkedValue = selectedSize.sku ? selectedSize.sku : '';

		return (
			<div>
				<EcommerceBar
					title={title}
					selectedVariant={selectedVariant}
					onProductSubmit={this.onProductSubmit.bind(this)}
					onSaveForLaterSubmit={this.onSaveForLaterSubmit.bind(this)}
					onNotifySubmit={this.onNotifySubmit.bind(this)}
				/>

				<VariantsCarousel
					images={product.galleryImages}
					selectedSlide={selectedSlideIndex}
				/>

				<VariantWrapper>
					<VariantRadios
						name={'productVariants'}
						selectedValue={selectedVariant.sku}
						onChange={this.onVariantChange.bind(this)}
						colors={colors}
					/>

					<VariantSizeSelector
						variant={selectedVariant}
						onChange={this.onSizeChange.bind(this)}
						checkedValue={checkedValue}
					/>

					<VariantActions
						selectedVariant={selectedVariant}
						selectedSize={selectedSize}
						onProductSubmit={this.onProductSubmit.bind(this)}
						onSaveForLaterSubmit={this.onSaveForLaterSubmit.bind(this)}
						onNotifySubmit={this.onNotifySubmit.bind(this)}
					/>
				</VariantWrapper>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	handleSizeChange: sku => dispatch(handleSizeChange(sku)),
	handleVariantChange: (sku, size) => dispatch(handleVariantChange(sku, size)),
	setCurrentVariant: variant => dispatch(setCurrentVariant(variant)),
	setProductOnLoad: (product) => dispatch(setProductOnLoad(product)),
	setSelectedSlide: (slideIndex) => dispatch(setSelectedSlide(slideIndex))
})

const mapStateToProps = state => ({
	selectedVariant = {},
	selectedSize = {},
	selectedSlide = '',
	selectedSlideIndex = 0
}) => ({
	selectedVariant,
	selectedSize,
	selectedSlide,
	selectedSlideIndex
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(VarinatSelector);
