// theirs
import { h, Component } from 'preact';
import { HeaderRow, HeaderMeta, HeaderTitle } from '@components/header';

import { connect } from 'react-redux';

class EcommerceBar extends Component {

	constructor(props) {
		super(props)
	}

	onClick() {
		console.log('button clicked')
	}

	render({
		title = '',
		selectedVariant = {},
		selectedSize = {},
		onProductSubmit,
		onNotifySubmit
  	}) {

		const {
			price,
			wasPrice,
			sizes,
		} = selectedVariant;

		const notifyMe = !selectedSize ? Object.values(sizes).reduce((acc, curr) => acc ? !acc.notifyMe : curr.notifyMe, false) : selectedSize.notifyMe;
		const outOfStock = !selectedSize ? Object.values(sizes).reduce((acc, curr) => acc ? acc.stock : curr.stock < 1, false) : selectedSize.stock < 1;

		const buttonText = notifyMe && outOfStock
			? 'Notify Me When Available'
			: outOfStock
			? 'Out Of Stock'
			: 'Add To Basket';

		const isDisabled = notifyMe && outOfStock
			? false
			: outOfStock;

		return (
			<HeaderRow>
				<HeaderTitle>
					{title}
				</HeaderTitle>
				<HeaderMeta
					buttonDisabled={isDisabled}
					buttonOnClick={notifyMe ? onNotifySubmit : onProductSubmit}
					buttonText={buttonText}
					price={price.formattedValue}
					wasPrice={wasPrice.formattedValue}
					selectedSize={selectedSize}
				/>
			</HeaderRow>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	handleSizeChange: sku => dispatch(handleSizeChange(sku)),
	handleVariantChange: sku => dispatch(handleVariantChange(sku)),
	setCurrentVariant: variant => dispatch(setCurrentVariant(variant))
})

const mapStateToProps = state => ({
  selectedSize = {},
  selectedVariant = {}
}) => ({
	selectedSize,
	selectedVariant
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EcommerceBar);
