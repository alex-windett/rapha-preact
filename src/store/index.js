
import { createStore, applyMiddleware, compose } from 'redux';
import { actions } from '@constants';

const {
	SET_CURRENT_VARIANT,
	HANDLE_SIZE_CHANGE,
	HANDLE_VARIANT_CHANGE,
	SET_PRODUCT_ON_LOAD,
	SET_SELECTED_SLIDE_ON_LOAD
} = actions


let ACTIONS = {
	HANDLE_VARIANT_CHANGE: (state, action) => {
		const { sku, previousSize } = action;

		const _size = Object.values(state.product.colors[sku].sizes).filter(size =>
			size.sanitizedSize === previousSize);

		return {
			...state,
			selectedVariant: state.product.colors[sku],
			selectedSize: _size[0],
		}
	},
	HANDLE_SIZE_CHANGE: (state, action) => {
		const { sku } = action;

		return {
			...state,
			selectedSize: state.selectedVariant.sizes[sku]
		}
	},
	SET_CURRENT_VARIANT: (state, action) => {
		const { variant } = action;

		return {
			...state,
			selectedVariant: variant
		}
	},
	SET_SELECTED_SLIDE_ON_LOAD: (state, action) => {

		const { slideIndex } = action;

		return {
			...state,
			selectedSlideIndex: slideIndex
		}
	},
	SET_PRODUCT_ON_LOAD: (state, action) => {
		const { product } = action;

		const productSizes = Object.values(product.colors).map(color => Object.values(color.sizes).map(size => size)).reduce( (curr, acc) => curr.concat(acc), []);
		const selectedSize = productSizes.filter( size => size.sku === product.selectedProductCode)[0];

		return {
			...state,
			product,
			selectedSize
		}
	}
};

const initialState = {
	product: {}
};

const middleware = [];

const composeEnhancers =
	typeof window === 'object' &&
	window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
		window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
		}) : compose;

const enhancer = composeEnhancers(
	applyMiddleware(...middleware),
);

export default createStore(
	(state = {}, action) => action && ACTIONS[action.type] ? ACTIONS[action.type](state, action) : state,
	initialState,
	enhancer
);
