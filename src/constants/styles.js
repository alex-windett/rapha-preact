const baseDim = 1.5;
let dims = {
	'dimQuarter': `${baseDim / 4}rem`,
	'dimHalf': `${baseDim / 2}rem`
}
for	( var i = 1; i < 10; i++ ) {
	dims = {
		...dims,
		[`dim${i}`]: `${baseDim * i}rem`
	}
}

const colors = {
	'black': '#000',
	'white': '#fff',
	'lightest-grey': '#f2f2f2',
	'light-grey': 'rgba(0, 0, 0, 0.15)',
	'grey': '#999',
	'pink': '#f67599',
	'dark-grey': '#2f2f2f',
	'available': '#7ed321',
	'unavailable': '#f00'
}

export default {
	dims,
	colors
}
