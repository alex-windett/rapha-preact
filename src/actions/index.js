import { actions } from '@constants';

const {
	SET_CURRENT_VARIANT,
	HANDLE_SIZE_CHANGE,
	HANDLE_VARIANT_CHANGE,
	SET_PRODUCT_ON_LOAD,
	SET_SELECTED_SLIDE_ON_LOAD
} = actions;

export const setCurrentVariant = (variant) => {
	return {
		type: SET_CURRENT_VARIANT,
		variant
	}
}

export const handleSizeChange = (sku) => ({
	type: HANDLE_SIZE_CHANGE,
	sku
})

export const handleVariantChange = (sku, previousSize = '') => ({
	type: HANDLE_VARIANT_CHANGE,
	sku,
	previousSize
})

export const setProductOnLoad = (product) => ({
	type: SET_PRODUCT_ON_LOAD,
	product
})

export const setSelectedSlide = (slideIndex) => ({
	type: SET_SELECTED_SLIDE_ON_LOAD,
	slideIndex
})
