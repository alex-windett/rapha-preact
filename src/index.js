import { h, Component } from 'preact';
import habitat from 'preact-habitat';
import AppContainer from '@containers/app';
import store  from '@store';

import { Provider } from 'react-redux';

require('offline-plugin/runtime').install();

const App = (props) => {

	return (
		<Provider store={store}>
			<AppContainer product={props} />
		</Provider>
	)
}

function init() {
	let niceLogin = habitat(App);

	niceLogin.render({
		inline: true,
		clean: false
	});
}

// in development, set up HMR:
if (module.hot) {
	require('preact/devtools'); // enables React DevTools, be careful on IE
	module.hot.accept('./containers/app', () => requestAnimationFrame(init));
}

init();
